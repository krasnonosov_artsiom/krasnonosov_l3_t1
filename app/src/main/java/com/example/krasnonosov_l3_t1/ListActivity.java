package com.example.krasnonosov_l3_t1;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerViewAdapter adapter;
    private RecyclerView.LayoutManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        ArrayList<RecyclerViewItem> items = new ArrayList<>();
        items.add(new RecyclerViewItem("True history of one men", "It was an early Sunday morning. Michael woke up at 6 in the morning," +
                " made coffee and went to the balcony to contemplate the down...", 4));
        items.add(new RecyclerViewItem("True history of one men", "It was an early Sunday morning. Michael woke up at 6 in the morning," +
                " made coffee and went to the balcony to contemplate the down...", 4));
        items.add(new RecyclerViewItem("True history of one men", "It was an early Sunday morning. Michael woke up at 6 in the morning," +
                " made coffee and went to the balcony to contemplate the down...", 4));
        items.add(new RecyclerViewItem("True history of one men", "It was an early Sunday morning. Michael woke up at 6 in the morning," +
                " made coffee and went to the balcony to contemplate the down...", 4));
        items.add(new RecyclerViewItem("True history of one men", "It was an early Sunday morning. Michael woke up at 6 in the morning," +
                " made coffee and went to the balcony to contemplate the down...", 4));
        items.add(new RecyclerViewItem("True history of one men", "It was an early Sunday morning. Michael woke up at 6 in the morning," +
                " made coffee and went to the balcony to contemplate the down...", 4));
        items.add(new RecyclerViewItem("True history of one men", "It was an early Sunday morning. Michael woke up at 6 in the morning," +
                " made coffee and went to the balcony to contemplate the down...", 4));
        items.add(new RecyclerViewItem("True history of one men", "It was an early Sunday morning. Michael woke up at 6 in the morning," +
                " made coffee and went to the balcony to contemplate the down...", 4));

        recyclerView = findViewById(R.id.recyclerView);
        adapter = new RecyclerViewAdapter(items, this);
        manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
    }
}
