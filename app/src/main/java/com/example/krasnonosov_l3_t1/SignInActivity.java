package com.example.krasnonosov_l3_t1;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SignInActivity extends AppCompatActivity {

    Button signInButton;
    EditText editText1;
    EditText editText2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        signInButton = findViewById(R.id.signInButton2);
        final Drawable defaultButtonBackground = signInButton.getBackground();
        signInButton.setBackgroundResource(R.drawable.button_style1_background);
        editText1 = findViewById(R.id.loginEditText);
        editText2 = findViewById(R.id.passwordEditText);

        editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                signInButton.setBackground(defaultButtonBackground);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (editText1.getText().toString().equals("") || editText2.getText().toString().equals("")) {
                    signInButton.setBackgroundResource(R.drawable.button_style1_background);
                }
            }
        });

        editText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                signInButton.setBackground(defaultButtonBackground);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (editText1.getText().toString().equals("") || editText2.getText().toString().equals("")) {
                    signInButton.setBackgroundResource(R.drawable.button_style1_background);
                }
            }
        });

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (signInButton.getBackground() != defaultButtonBackground) {
                    Toast.makeText(getApplicationContext(), "Please, enter your credentials", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(getApplicationContext(), ListActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

}