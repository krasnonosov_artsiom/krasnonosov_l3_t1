package com.example.krasnonosov_l3_t1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignUpActivity extends AppCompatActivity {

    Button signUpButton;
    EditText editText1;
    EditText editText2;
    EditText editText3;
    EditText editText4;
    EditText editText5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        signUpButton = findViewById(R.id.signUpButton2);
        final Drawable defaultButtonBackground = signUpButton.getBackground();
        signUpButton.setBackgroundResource(R.drawable.button_style2_background);
        editText1 = findViewById(R.id.createPasswordEditText);
        editText2 = findViewById(R.id.confirmPasswordEditText);
        editText3 = findViewById(R.id.cityTextView);
        editText4 = findViewById(R.id.fullNameEditText);
        editText5 = findViewById(R.id.emailEditText);


        editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                signUpButton.setBackground(defaultButtonBackground);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (editText1.getText().toString().equals("") ||
                        editText2.getText().toString().equals("") ||
                        editText3.getText().toString().equals("") ||
                        editText4.getText().toString().equals("") ||
                        editText5.getText().toString().equals("")) {
                    signUpButton.setBackgroundResource(R.drawable.button_style2_background);
                }
            }
        });

        editText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                signUpButton.setBackground(defaultButtonBackground);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (editText1.getText().toString().equals("") ||
                        editText2.getText().toString().equals("") ||
                        editText3.getText().toString().equals("") ||
                        editText4.getText().toString().equals("") ||
                        editText5.getText().toString().equals("")) {
                    signUpButton.setBackgroundResource(R.drawable.button_style2_background);
                }
            }
        });

        editText3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                signUpButton.setBackground(defaultButtonBackground);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (editText1.getText().toString().equals("") ||
                        editText2.getText().toString().equals("") ||
                        editText3.getText().toString().equals("") ||
                        editText4.getText().toString().equals("") ||
                        editText5.getText().toString().equals("")) {
                    signUpButton.setBackgroundResource(R.drawable.button_style2_background);
                }
            }
        });

        editText4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                signUpButton.setBackground(defaultButtonBackground);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (editText1.getText().toString().equals("") ||
                        editText2.getText().toString().equals("") ||
                        editText3.getText().toString().equals("") ||
                        editText4.getText().toString().equals("") ||
                        editText5.getText().toString().equals("")) {
                    signUpButton.setBackgroundResource(R.drawable.button_style2_background);
                }
            }
        });

        editText5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                signUpButton.setBackground(defaultButtonBackground);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (editText1.getText().toString().equals("") ||
                        editText2.getText().toString().equals("") ||
                        editText3.getText().toString().equals("") ||
                        editText4.getText().toString().equals("") ||
                        editText5.getText().toString().equals("")) {
                    signUpButton.setBackgroundResource(R.drawable.button_style2_background);
                }
            }
        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (signUpButton.getBackground() != defaultButtonBackground) {
                    Toast.makeText(getApplicationContext(), "Please, enter your credentials", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(getApplicationContext(), ListActivity.class);
                    startActivity(intent);
                }
            }
        });

    }
}
