package com.example.krasnonosov_l3_t1;

import android.widget.RatingBar;

public class RecyclerViewItem {

    private String title;
    private String description;
    private float rating;

    public RecyclerViewItem(String title, String description, float rating) {
        this.title = title;
        this.description = description;
        this.rating = rating;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public float getRating() {
        return rating;
    }
}
